﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oAlumno
{
    public class ol
    {
        private string matricula;

        public string Matricula
        {
            get { return matricula; }
            set { matricula = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string fechaNacimiento;

        public string FechaNacimiento
        {
            get { return fechaNacimiento; }
            set { fechaNacimiento = value; }
        }
        private string semestre;

        public string Semestre
        {
            get { return semestre; }
            set { semestre = value; }
        }
        private string facultad;

        public string Facultad
        {
            get { return facultad; }
            set { facultad = value; }
        }
        private string municipio;

        public string Municipio
        {
            get { return municipio; }
            set { municipio = value; }
        }
        private string codigo;

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }
        private string nomMunicipio;

        public string NomMunicipio
        {
            get { return nomMunicipio; }
            set { nomMunicipio = value; }
        }
    }
}
