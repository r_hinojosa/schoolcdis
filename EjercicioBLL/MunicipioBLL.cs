﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CapaDeDatos;

namespace CapaLogica
{
    public class MunicipioBLL
    {

        public DataTable consultar()
        {

            MunicipioDAL mun = new MunicipioDAL();
            DataTable dt = new DataTable();
            dt = mun.consultar();
            return dt;
        }


        public DataTable consultarEstado(string id)
        {

            MunicipioDAL mun3 = new MunicipioDAL();
            DataTable dt = new DataTable();
            dt = mun3.consultarEstado(id);
            return dt;
        }

        // Solo sirve para alumno_d
        public DataTable consultarEstadoUpdate(string id)
        {
            MunicipioDAL mun4 = new MunicipioDAL();
            DataTable dt = new DataTable();
            dt = mun4.consultarEstadoUpdate(id);
            return dt;

        }


        public DataTable SearchEstadoUpdate(string estado)
        {

            MunicipioDAL mun5 = new MunicipioDAL();
            DataTable dt = new DataTable();
            dt = mun5.SearchEstadoUpdate(estado);
            return dt;
        
        }
    }
}
