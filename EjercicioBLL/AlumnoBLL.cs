﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CapaDeDatos;

namespace CapaLogica
{
    public class AlumnoBLL

    {
       
        public bool centinela;
        public bool centinela2;
        
        #region alumno_u.alumno_s 

        public DataTable consultar()
        { 
       
            catalogoAlumnoDAL alumnos = new catalogoAlumnoDAL();
            return alumnos.consultar();
          
        }

        public DataTable modificar (string matricula, string nombre, int? semestre, string fechaNacimiento,string facultad, string municipio)
            
       {
             catalogoAlumnoDAL alumno1 = new catalogoAlumnoDAL();
             return alumno1.modificar(matricula,  nombre,  semestre, fechaNacimiento, facultad, municipio);

        }

        public DataTable BuscarId (int id)
        { 
            catalogoAlumnoDAL alumnos2 = new catalogoAlumnoDAL();
            return alumnos2.FindById(id);
           
        }

        public DataTable CargarFacultad ()
        {

            catalogoAlumnoDAL alumno3 = new catalogoAlumnoDAL();
            return alumno3.cargarFacultades();
        
        }

        public DataTable FindAlumno(string matricula)
        {
            catalogoAlumnoDAL alumno4 = new catalogoAlumnoDAL();
            return alumno4.BuscarAlumnoU(matricula);
        
        }

        #endregion

        #region alumno_i

       
        public void SaveAlumno(string matricula, string nombre, string semestre, string fechaNacimiento, string facultad, string municipio)
        {

            //Revisar que no exista un alumno con la misma matrícula
            DataTable dt = new DataTable();
            catalogoAlumnoDAL alumno5 = new catalogoAlumnoDAL();
            dt = FindAlumno(matricula);

            if (dt.Rows.Count == 0)
            {

                DataTable dt5 = new DataTable();
                dt5 = alumno5.buscarNumMunicipios(municipio);
                int numFilas = int.Parse(dt5.Rows[0]["MunId"].ToString());

                //Limite de Alumnos por Municipios 
                if (numFilas < 5)
                {
                    this.centinela2 = true;
                   
                }
                else
                {
                    this.centinela2 = false;
                    throw new Exception("No se puede guardar el alumno, excede el limite de cupo por municipio (5)");
                }

                //Limite de fechas de nacimiento
                string limiteFecha = "1950/01/01";

                if (DateTime.Parse(fechaNacimiento) > DateTime.Parse(limiteFecha))
                {
                    this.centinela = true;
                    
                }
                else
                {
                    this.centinela = false;
                    throw new Exception("El Alumno excede de la edad permitida");

                }

                // Si los dos son verdaderos se guarda el alumno
                if (this.centinela == true && this.centinela2 == true)
                {
                    alumno5.guardarAlumnoi(matricula, nombre, semestre, fechaNacimiento, facultad, municipio);

                }
                else
                {
                   //Si no, no se puede guardar el alumno 
                }


            }


            else
            {
                throw new Exception("Ya existe un alumno registrado con esta matrícula");

            }


        }


        //Verificar numero de personas en un municipio

        public DataTable buscarNumMunicipios(string num)
        {

            catalogoAlumnoDAL alu7 = new catalogoAlumnoDAL();
            DataTable dt7 = new DataTable();
            dt7 = alu7.buscarNumMunicipios(num);
            return dt7;
        
        
        }


        #endregion

        #region alumno_d
        public DataTable EliminarAlumno(string matricula)
        {

            catalogoAlumnoDAL alumno6 = new catalogoAlumnoDAL();
            return alumno6.Eliminaralumno(matricula);
        
        }
        #endregion

    }
}
