﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CapaDatosFac;


namespace CapaLogicaFacultad
{
    public class FacultadBLL
    {
        #region facultad_d
       
        public DataTable FindFacId(int id)
        {

            catalogoFacDAL fac = new catalogoFacDAL();
            return fac.BuscarFacId(id);
        }
        public DataTable FindByCod(string codigo)
        {

            catalogoFacDAL fac2 = new catalogoFacDAL();
            return fac2.BuscarFaCod(codigo);
        
        }

        public DataTable EraseFac(string codigo)

        {
            catalogoFacDAL fac3 = new catalogoFacDAL();
            return fac3.EliminarFac(codigo);
        }
        #endregion

        #region facultad_i

        public void SaveFac(string facultad, string codigo )
        
        {
            // Exepcion para que no se repita el mismo codigo 
            DataTable dt = new DataTable();
            catalogoFacDAL fac4 = new catalogoFacDAL();

            dt = FindByCod(codigo);
            if (dt.Rows.Count == 0)
            {
               
                fac4.GuardarFac(facultad, codigo);
            }
            else
            {
                throw new Exception("Ya existe una facultad registrada con ese codigo");

            }

        

        }   
        #endregion

        #region facultad_s

        public DataTable LoadFac()
        {
            catalogoFacDAL fac5 = new catalogoFacDAL();
            return fac5.cargarFac();

        }

        #endregion

        #region facultad_u

        public void ModifyFac(string facultad, string codigo)
        {
            catalogoFacDAL fac6 = new catalogoFacDAL();
            fac6.modificarFac(facultad, codigo);
        
        }

        #endregion
    }
}
