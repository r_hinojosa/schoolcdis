﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GlobalesBLL;
using CapaDeDatos;

namespace CapaLogica
{
    public class EstadoBLL
    {
        public DataTable consultar()
        {
            EstadoDAL estado = new EstadoDAL();
            DataTable dt = new DataTable();

            dt = estado.consultar();
            return dt;
        }

        public DataTable consultarPais(string idEdo)
        {

            EstadoDAL estado2 = new EstadoDAL();
            DataTable dt2 = new DataTable();

            dt2 = estado2.consultarPais(idEdo);
            return dt2;
        
    }

        public DataTable consultarPaisI(string idEdo)
        {

            EstadoDAL estad3 = new EstadoDAL();
            DataTable dt3 = new DataTable();

            dt3 = estad3.consultarPaisI(idEdo);
            return dt3;
        
        }
        //USADO PARA ALUMNO_U
        public DataTable SearchPaisUpdate(string pais)
        {

            EstadoDAL estad3 = new EstadoDAL();
            DataTable dt3 = new DataTable();

            dt3 = estad3.SearchPaisUpdate(pais);
            return dt3;
        
        }

    }
}
