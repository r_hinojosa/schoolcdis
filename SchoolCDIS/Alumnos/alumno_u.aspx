﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master"  CodeBehind="alumno_u.aspx.cs" Inherits="SchoolCDIS.Alumnos.alumno_u" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Styles/Site.css" rel="stylesheet" />

<script type="text/javascript">
        $(function() {
            $('#MainContent_txtFechaNac').datepick();
           
        });    
    </script>
    <style type="text/css">
        .auto-style2 {
            width: 109px;
        }
        .auto-style3 {
            height: 30px;
        }
        .auto-style4 {
            width: 109px;
            height: 30px;
        }
        .auto-style5 {
            height: 26px;
        }
        .auto-style6 {
            width: 109px;
            height: 26px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <br />

        <p align="center"  style ="font-size:25px; font-family:'Trebuchet MS';color:black; background-color:aqua; ">
              Modificar Alumno
        </p>
        <br />
    </div>
<table align="center" id="tablaAltas" >
<tr>
<td  class="auto-style3"><asp:Label ID="lblMatricula" runat="server" Text="Matricula" Font-Bold="True" Font-Italic="False" Font-Names="Trebuchet MS" ForeColor="Black"></asp:Label></td>
<td class="auto-style4"><asp:TextBox ID="txtMatricula" runat="server" Width="136px"></asp:TextBox></td>
<td><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ValidationGroup="mat" ErrorMessage="Ingrese la Matricula" ControlToValidate ="txtMatricula" ForeColor=Red  Display="Dynamic" Text="*"></asp:RequiredFieldValidator>   
<asp:RegularExpressionValidator ID="RegularExpressionValidator1"  ValidationGroup="mat" runat="server" ErrorMessage="Solo se aceptan numeros" Text="*" ControlToValidate ="txtMatricula" ValidationExpression = "^\d+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator> </td> </td>
<td class="auto-style3"> &nbsp;</td>
</tr>
<tr>
<td ><asp:Label ID="lblNombre" runat="server" Text="Nombre" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="Black"></asp:Label></td>
<td class="auto-style2"><asp:TextBox ID="txtNombre" runat="server" Width="140px"></asp:TextBox></td>
<td> &nbsp;</td>
</tr>
<tr>
<td ><asp:Label ID="lblFechaNac" runat="server" Text="Fecha de nacimiento" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="Black"></asp:Label>
    </td>
<td ><asp:TextBox ID="txtFechaNac" runat="server" Width="138px"></asp:TextBox></td>
    <td> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Ingrese su fecha de nacimiento" Text ="*" Display="Dynamic" ControlToValidate ="txtFechaNac" ValidationGroup="mat"  ForeColor="Red" ></asp:RequiredFieldValidator>
        <asp:CustomValidator ID="cvFechaNac" runat="server" ErrorMessage="Ingrese el formato de Fecha Valido (yyyy/MM/dd)" Display="Dynamic" ForeColor="Red" ControlToValidate ="txtFechaNac" ValidationGroup="mat"  ClientValidationFunction ="validarFecha"  OnServerValidate="cvFechaNac_ServerValidate" Text="*"></asp:CustomValidator>
<asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtFechaNac" MinimumValue="1900/01/01" MaximumValue="1999/01/01" Type="Date" ValidationGroup="mat" runat="server"  Text="*" ErrorMessage="Es menor de 16 años" ForeColor="Red" Display="Dynamic"></asp:RangeValidator>&nbsp;&nbsp;<input type="image" id="Text1"  src="/Imagenes/calendar.png"  runat="server" />&nbsp; &nbsp;&nbsp; 


</td>
   
</tr>
<tr>
<td ><asp:Label ID="lblSemestre" runat="server" Text="Semestre" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="Black"></asp:Label></td>
<td class="auto-style2"><asp:TextBox ID="txtSemestre" runat="server" Width="142px"></asp:TextBox></td>
<td> 
    <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="Semestre fuera de rango" MinimumValue="1" MaximumValue="12" Display="Dynamic" ForeColor="Red" ControlToValidate ="txtSemestre" ValidationGroup="mat" Text="*"></asp:RangeValidator>  </td>
</tr>
<tr>
<td class="auto-style5" ><asp:Label ID="lblFacultad" runat="server" Text="Facultad" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="Black"></asp:Label>
   
    </td>
    <%--<td><asp:TextBox ID="txtFacultad" runat="server"></asp:TextBox></td>--%>
<td class="auto-style6">
    <asp:DropDownList ID="ddlFacultad" runat="server" Height="20px" Width="148px">
    </asp:DropDownList> 
  
    </td>
    <td class="auto-style5"> &nbsp;&nbsp;</td>
</tr>
    <tr><td>
    <asp:Label ID="lblPais" runat="server" Text="Pais"></asp:Label></td>
    <td>
        <asp:DropDownList ID="ddlPais" runat="server" Height="19px" 
            Width="151px" OnSelectedIndexChanged="ddlPais_SelectedIndexChanged" AutoPostBack="True" ViewStateMode="Enabled"  EnableViewState="true">
        </asp:DropDownList>    </td>
    </tr>

    <tr><td>
    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label></td>
    <td>
        <asp:DropDownList ID="ddlEstado" runat="server" Height="19px" 
             Width="151px" OnSelectedIndexChanged="ddlEstado_SelectedIndexChanged"
            AutoPostBack="True" ViewStateMode="Enabled"  EnableViewState="true">
        </asp:DropDownList>    </td>
    </tr>

    <tr><td>
    <asp:Label ID="lblMunicipio" runat="server" Text="Municipio"></asp:Label></td>
    <td>
        <asp:DropDownList ID="ddlMunicipio" runat="server" Height="19px" 
             Width="151px" AutoPostBack="True" ViewStateMode="Enabled"  EnableViewState="true">
        </asp:DropDownList>    </td>
    </tr>
<tr>
<td >
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
        onclick="btnBuscar_Click" Width="78px" BackColor="#339966" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="White"  /></td>
<td align = "right" class="auto-style2">
    &nbsp;&nbsp; <asp:Button ID="btnModificar" runat="server" Text="Modificar" 
        onclick="btnModificar_Click" Width="77px" BackColor="#CC3300" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="White" ValidationGroup="mat" /></td>
</tr>
</table>
    <br>
    <asp:ValidationSummary Font-Bold="True" ID="ValidationSummary1" runat="server"  align ="center" ValidationGroup="mat" HeaderText="Debe de entrar los siguientes campos:" DisplayMode=List EnableClientScript="true" ForeColor=Red/>
 <script type="text/javascript">

     function validarFecha(source, arguments) {
         var fecha = arguments.Value;

         if (!/^(19|20)\d\d[\/](0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])$/.test(fecha)) {
             arguments.IsValid = false;
         }
         else {
             var parts = fecha.split("/");
             var day = parseInt(parts[2], 10);
             var month = parseInt(parts[1], 10);
             var year = parseInt(parts[0], 10);

             var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

             if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
                 monthLength[1] = 29;
             }
             if (day > 0 && day <= monthLength[month - 1]) {
                 arguments.IsValid = true;

             }
             else {
                 arguments.IsValid = false;
             }
         }

     }

</script>
    &nbsp;
    <br />
    <br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
&nbsp;
</asp:Content>
