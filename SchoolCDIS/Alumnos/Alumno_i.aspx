﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Alumno_i.aspx.cs" Inherits="SchoolCDIS.Alumnos.Alumno_i" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 128px;
        }
    </style>
  <script type="text/javascript">
        $(function () {
           $('#MainContent_txtFechaNac').datepick();

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div>
    <br />
    <p align="center"  style ="font-size:25px; font-family:'Trebuchet MS';color:black; background-color:aqua;">
             Registrar Alumno
        </p>
    <br />
    <br />

</div>
<table  id="tablaAltas" align=center>
<tr>
<td><asp:Label ID="lblMatricula" runat="server" Text="Matricula" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="Black"></asp:Label></td>
<td class="auto-style1"><asp:TextBox ID="txtMatricula" runat="server" MaxLength="7" ></asp:TextBox></td>


      
<td>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ValidationGroup="mat" ErrorMessage="Ingrese la Matricula" ControlToValidate ="txtMatricula" ForeColor="Red " Display="Dynamic" Text="*"></asp:RequiredFieldValidator>   
<asp:RegularExpressionValidator ID="RegularExpressionValidator1"  ValidationGroup="mat" runat="server" ErrorMessage="Solo se aceptan numeros" Text="*" ControlToValidate ="txtMatricula" ValidationExpression = "^\d+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator> 
      <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="La matricula debe contener 7 digitos" ControlToValidate="txtMatricula" ValidationGroup="mat" ForeColor="Red " Display="Dynamic" Text="*" 
ValidationExpression="\d{7}"></asp:RegularExpressionValidator></td>  
    
    
     <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate ="txtMatricula"  ValidationExpression =""></asp:RegularExpressionValidator>--%>
   <%-- <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" OnServerValidate=""></asp:CustomValidator>--%>
</tr>
<tr>
<td><asp:Label ID="lblNombre" runat="server" Text="Nombre" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td class="auto-style1"><asp:TextBox ID="txtNombre" runat="server"></asp:TextBox></td>
<td>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Ingrese un nombre" ControlToValidate="txtNombre" ValidationGroup="mat" Display="Dynamic" Text="*"  ForeColor ="Red"></asp:RequiredFieldValidator>  </td>
</tr>
<tr>
<td><asp:Label ID="lblFechaNac" runat="server" Text="Fecha de nacimiento" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td class="auto-style1"><asp:TextBox ID="txtFechaNac" runat="server"></asp:TextBox></td>
<td><asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtFechaNac" MinimumValue="1900-01-01" MaximumValue="1999-01-01" Type="Date" ValidationGroup="mat" runat="server"  Text="*" ErrorMessage="Es menor de 16 años" ForeColor="Red" Display="Dynamic"></asp:RangeValidator>
     <asp:CustomValidator ID="cvFechaNac" runat="server" ErrorMessage="Ingrese el formato de Fecha Valido (yyyy/MM/dd)" Display="Dynamic" ForeColor="Red" ControlToValidate ="txtFechaNac" ValidationGroup="mat" OnServerValidate="cvFechaNac_ServerValidate" ClientValidationFunction ="validarFecha" Text="*"></asp:CustomValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Ingrese la fecha de nacimiento" Display="Dynamic" ForeColor="Red" ControlToValidate ="txtFechaNac" ValidationGroup="mat" Text="*"></asp:RequiredFieldValidator></td>

</tr>
<tr>
<td><asp:Label ID="lblSemestre" runat="server" Text="Semestre" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td class="auto-style1"><asp:TextBox ID="txtSemestre" runat="server"></asp:TextBox></td>
<td> 
    <asp:RangeValidator ID="RangeValidator2" Type="Integer" runat="server" ErrorMessage="Semestre fuera de rango" MinimumValue="1" MaximumValue="12" Display="Dynamic" ForeColor="Red" ControlToValidate ="txtSemestre" ValidationGroup="mat" Text="*"></asp:RangeValidator>  

    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Ingrese el Semestre" Display="Dynamic" ForeColor="Red" ControlToValidate ="txtSemestre" ValidationGroup="mat" Text="*"></asp:RequiredFieldValidator>
</td>
</tr>
<tr>
<td><asp:Label ID="lblFacultad" runat="server" Text="Facultad" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<%--<td><asp:TextBox ID="txtFacultad" runat="server"></asp:TextBox></td>--%>
<td class="auto-style1">
    <asp:DropDownList ID="ddlFacultad" runat="server" 
        AutoPostBack="true" Height="19px" Width="151px"></asp:DropDownList></td>
        <td>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2"  ValidationGroup="mat" runat="server" ErrorMessage="Ingrese la Facultad" ControlToValidate ="ddlFacultad" ForeColor=Red Text="*"></asp:RequiredFieldValidator>   </td>

</tr>


<tr><td>
    <asp:Label ID="lblPais" runat="server" Text="Pais"></asp:Label></td>
    <td>
        <asp:DropDownList ID="ddlPais" runat="server" Height="19px"  Width="151px" 
            AutoPostBack="True" ViewStateMode="Enabled"  EnableViewState="true" 
            onselectedindexchanged="ddlPais_SelectedIndexChanged">
        </asp:DropDownList></td>
    </tr>

    <tr><td>
    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label></td>
    <td>
        <asp:DropDownList ID="ddlEstado" runat="server" Height="19px" Width="151px" AutoPostBack="True" OnSelectedIndexChanged="ddlEstado_SelectedIndexChanged">
        </asp:DropDownList>    </td>
    </tr>

    <tr><td>
    <asp:Label ID="lblMunicipio" runat="server" Text="Municipio"></asp:Label></td>
    <td>
        <asp:DropDownList ID="ddlMunicipio" runat="server" Height="19px" Width="151px" 
            >
        </asp:DropDownList>    </td>
    </tr>

<td></td>
<td align = "right" class="auto-style1">
    <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" 
        onclick="btnAceptar_Click" BackColor="#339933" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="White"  ValidationGroup="mat"  />
</td>
</tr>
</table>
<br />
<div>

    <asp:GridView  ID="grdAlumno" runat="server" 
        Width="593px" AutoGenerateColumns = "false" Height="203px"  align="center" 
        Font-Bold="True" Font-Names="Trebuchet MS"  >
      
        <AlternatingRowStyle BackColor="#0033CC" ForeColor="White" />
      
    <Columns>

    <asp:BoundField DataField ="numMatricula" HeaderText="Matrícula" />
    <asp:BoundField DataField ="nombre" HeaderText="Nombre" />
    <asp:BoundField DataField ="fechaNacimiento" HeaderText="Fecha de nacimiento"   DataFormatString="{0:yyyy/MM/dd}"/>
    <asp:BoundField DataField ="semestre" HeaderText="Semestre" />
    <asp:BoundField DataField ="facultad" HeaderText="Facultad" />
   <%--  DataField es donde va el alias en el query del JOIN  --%> 
   <asp:BoundField DataField ="Municipio" HeaderText="Municipio" />
    </Columns>
        <EditRowStyle Font-Bold="True" Font-Names="Stencil" ForeColor="White" />
        <HeaderStyle BackColor="Black" ForeColor="White" />
        <RowStyle BackColor="#333399" ForeColor="White" />
    </asp:GridView>

</div>



<script type="text/javascript">

    function validarFecha(source, arguments) 
    {
        var fecha = arguments.Value;

        if (!/^(19|20)\d\d[\/](0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])$/.test(fecha))
            {
                arguments.IsValid = false;
            }
        else 
                {
                    var parts = fecha.split("/");
                    var day = parseInt(parts[2], 10);
                    var month = parseInt(parts[1], 10);
                    var year = parseInt(parts[0], 10);

                    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                    {
                        monthLength[1] = 29;
                    }
                    if (day > 0 && day <= monthLength[month - 1]) 
                    {
                        arguments.IsValid = true;

                    }
                    else 
                    {
                        arguments.IsValid = false;
                    }
                }

    }

</script>
<br />
<div align = "center">
<asp:ValidationSummary  Font-Bold="True" ValidationGroup="mat" ID="ValidationSummary1" runat="server" HeaderText="Debe de entrar los siguientes campos:" DisplayMode=List EnableClientScript="true" ForeColor=Red ></asp:ValidationSummary >
</div>

</asp:Content>
