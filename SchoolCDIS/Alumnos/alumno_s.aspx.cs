﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CapaLogica;

namespace SchoolCDIS.Alumnos
{
    public partial class alumno_s : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cargarAlumnos(); 
            }
            
        }
        #region Eventos
                     //N/A
        #endregion
       
        #region Metodos
        private void cargarAlumnos()
        {


            AlumnoBLL alumnos = new AlumnoBLL();
            DataTable dt = new DataTable();
            dt = alumnos.consultar();

            grdAlumno.DataSource = dt;
            grdAlumno.DataBind();

        }
        protected void grdAlumno_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Modificar")
            {
                Response.Redirect("~/Alumnos/alumno_u.aspx?pId=" + e.CommandArgument.ToString());
            }
            if (e.CommandName == "Eliminar")
            {
                Response.Redirect("~/Alumnos/alumno_d.aspx?pId=" + e.CommandArgument.ToString());
            }
        }
        #endregion
    

        

    }
}