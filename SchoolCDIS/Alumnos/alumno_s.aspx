﻿<%@ Page enableEventValidation="false" validateRequest="false" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="alumno_s.aspx.cs" Inherits="SchoolCDIS.Alumnos.alumno_s"   %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" style ="z-index:999">
    <link href="/Styles/Site.css" rel="stylesheet" type="text/css" />
        <div>
        <br />
            <p align="center"  style ="font-size:25px; font-family:'Trebuchet MS';color:black; background-color:aqua; padding-top:0px;">
              Tabla de Alumnos
        </p>
        <br />
        <br />
            
    </div>
    <asp:GridView  ID="grdAlumno" runat="server" onrowcommand="grdAlumno_RowCommand" 
        Width="593px" AutoGenerateColumns = "false" Height="203px"  align="center" 
        Font-Bold="True" Font-Names="Trebuchet MS"  >
      
        <AlternatingRowStyle BackColor="#0033CC" ForeColor="White" />
      
    <Columns>
  <%--  <asp:TemplateField>
    <ItemTemplate>
    <asp:LinkButton ID="btnModificar" runat ="server" Text="Modificar"  CommandName = "Modificar" CommandArgument = '<%# Eval("alumnoid")%>'  >
    </asp:LinkButton>
    </ItemTemplate>
    </asp:TemplateField>--%>

 <%--   <asp:TemplateField>
    <ItemTemplate>
    <asp:LinkButton ID="btnEliminar" runat ="server" Text="Eliminar"  CommandName = "Eliminar" CommandArgument = '<%# Eval("alumnoid")%>'  >
    </asp:LinkButton>
    </ItemTemplate>
    </asp:TemplateField>--%>

    <asp:TemplateField>
    <ItemTemplate>
    <asp:ImageButton ID="btnEliminar" Width="20px" Height="20px" runat="server" CommandName = "Eliminar" CommandArgument = '<%# Eval("alumnoid")%>' ImageUrl="~/Imagenes/eliminar.png"  />
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <ItemTemplate>
    <asp:ImageButton ID="btnModificar" Width="20px" Height="20px" runat="server" CommandName = "Modificar" CommandArgument = '<%# Eval("alumnoid")%>' ImageUrl="~/Imagenes/editar.jpg"  />
    </ItemTemplate>
    </asp:TemplateField>

    <asp:BoundField DataField ="alumnoid" HeaderText="Id" Visible="false"/>
    <asp:BoundField DataField ="numMatricula" HeaderText="Matrícula" />
    <asp:BoundField DataField ="nombre" HeaderText="Nombre" />
    <asp:BoundField DataField ="fechaNacimiento" HeaderText="Fecha de nacimiento"   DataFormatString="{0:yyyy/MM/dd}"/>
    <asp:BoundField DataField ="semestre" HeaderText="Semestre" />
    <asp:BoundField DataField ="facultad" HeaderText="Facultad" />
   <%--  DataField es donde va el alias en el query del JOIN  --%> 
   <asp:BoundField DataField ="Municipio" HeaderText="Municipio" />
    </Columns>
        <EditRowStyle Font-Bold="True" Font-Names="Stencil" ForeColor="White" />
        <HeaderStyle BackColor="Black" ForeColor="White" />
        <RowStyle BackColor="#333399" ForeColor="White" />
    </asp:GridView>
    <div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />


    </div>
</asp:Content>
