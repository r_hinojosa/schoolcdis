﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Text.RegularExpressions;
using CapaLogica;


namespace SchoolCDIS.Alumnos
{
    public partial class alumno_u : System.Web.UI.Page
    {
        private int alumnoId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["pId"] != null)
                {

                    alumnoId = int.Parse(Request.QueryString["pId"]);
                    buscarAlumnoPorId(alumnoId);
                   
                    //cargarFacultades();

                }
            }

        }
        #region Eventos

        protected void cvFechaNac_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            //Pasar javascript aqui 
            string fecha = e.Value;
            string expReg = @"^((19|20)[0-9][0-9])/((0[1-9]|1[012]))/(0[1-9]|[12][0-9]|3[01])$";
            Match match = Regex.Match(fecha, expReg);

            //  if (!/^(19|20)\d\d[\/](0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])$/.test(fecha))
            if (!match.Success)
            {
                e.IsValid = false;
            }

            else
            {
                char[] delimitador = { '/' };
                string[] parts = fecha.Split(delimitador);
                int day = int.Parse(parts[2]);
                int month = int.Parse(parts[1]);
                int year = int.Parse(parts[0]);

                int[] monthLength = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };


                if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                {
                    monthLength[1] = 29;
                }
                if (day > 0 && day <= monthLength[month - 1])
                {
                    e.IsValid = true;

                }
                else
                {
                    e.IsValid = false;
                }
            }


        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
             // forzar validacion 
            if (cvFechaNac.IsValid)
            {
                string matricula = txtMatricula.Text;
                BuscarAlumno(matricula);
            }
        }


        protected void btnModificar_Click(object sender, EventArgs e)
        { // forzar validacion 
            if (cvFechaNac.IsValid)
            {
                modificarAlumno();
            }
        }
        
        protected void ddlPais_SelectedIndexChanged(object sender, EventArgs e)
        {

              if (ddlPais.SelectedIndex != 0)
            {

            //Limpiar primero estados
            ddlEstado.Items.Clear();

            //Cargamos estados 
            cargarEstados();

            ddlEstado.Enabled = true;
            ddlMunicipio.Enabled = false;

            }
              
              else

              {
                  ddlEstado.Items.Clear();
                  ddlMunicipio.Items.Clear();
                  ddlEstado.Enabled = false;
                  ddlMunicipio.Enabled = false;
             
             }
        }

        protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlMunicipio.SelectedIndex != 0)
            {
                //Limpiar primero municipios
                ddlMunicipio.Items.Clear();

                //Cargamos municipios
                cargarMunicipios();

                ddlMunicipio.Enabled = true;

            }
            else
            { 
                ddlMunicipio.Enabled = false; 
            
            }


        }



        #endregion


        #region Metodos



        public void cargarFacultades()
        {
            
            AlumnoBLL alumno3 = new AlumnoBLL();
            DataTable dt = new DataTable();

            dt = alumno3.CargarFacultad();

            ddlFacultad.DataSource = dt;
            ddlFacultad.DataTextField = "codigo";
            ddlFacultad.DataValueField = "FacultadId";
            ddlFacultad.DataBind(); //Dibujar tabla
            //ddlFacultad.Items.

        }

        private void cargarPaises()
        {
            PaisBLL fac = new PaisBLL();
            DataTable dt = new DataTable();

            dt = fac.consultar();

            ddlPais.DataSource = dt;
            ddlPais.DataTextField = "Nombre";
            ddlPais.DataValueField = "PaisId";
            ddlPais.DataBind();
            ddlPais.Items.Insert(0, new ListItem("<-Seleccione->", ""));


        }

        private void cargarEstados()
        {
            EstadoBLL edo = new EstadoBLL();
            DataTable dt5 = new DataTable();

            string valorPais = ddlPais.SelectedValue;

            dt5 = edo.consultarPaisI(valorPais);


            ddlEstado.DataSource = dt5;
            ddlEstado.DataTextField = "Nombre";
            ddlEstado.DataValueField = "EstadoId";
            ddlEstado.DataBind();
            ddlEstado.Items.Insert(0, new ListItem("<-Seleccione->", ""));
        }

        private void cargarMunicipios()
        {
            MunicipioBLL mun = new MunicipioBLL();
            DataTable dt = new DataTable();

            string valorEstado = ddlEstado.SelectedValue;

            dt = mun.consultarEstado(valorEstado);

            ddlMunicipio.DataSource = dt;
            ddlMunicipio.DataTextField = "Nombre";
            ddlMunicipio.DataValueField = "MunId";
            ddlMunicipio.DataBind();
            ddlMunicipio.Items.Insert(0, new ListItem("<-Seleccione->", "0"));
        }


       public void buscarAlumnoPorId(int id)
        
        {

           
            AlumnoBLL alumno2 = new AlumnoBLL();
            DataTable dt = new DataTable();
           
            dt = alumno2.BuscarId(id);
            
            txtMatricula.Text = dt.Rows[0]["numMatricula"].ToString();
            txtNombre.Text = dt.Rows[0]["nombre"].ToString();
            txtSemestre.Text = dt.Rows[0]["semestre"].ToString();

            cargarFacultades();


            ddlFacultad.Items.FindByValue(dt.Rows[0]["facultad"].ToString()).Selected = true;
         
           //Buscar Muicipios que solo coinidan  con el EstadoId 
          
            string estado = dt.Rows[0]["estado"].ToString();
           
            MunicipioBLL mun = new MunicipioBLL();
            DataTable dt2 = new DataTable();

            dt2 = mun.SearchEstadoUpdate(estado);

            ddlMunicipio.DataSource = dt2;
            ddlMunicipio.DataTextField = "Nombre";  //Nombre en el dropdownlist
            ddlMunicipio.DataValueField = "MunId"; //Valor del campo en el dropdownlist
            ddlMunicipio.DataBind();

           //Selecionar el municipio seleccionado 
            ddlMunicipio.Items.FindByValue(dt.Rows[0]["MunId"].ToString()).Selected = true;


            string fecha = dt.Rows[0]["fechaNacimiento"].ToString();
            DateTime fechax = Convert.ToDateTime(fecha);
            txtFechaNac.Text = fechax.ToString("yyyy/MM/dd");

           
           //Cargar Estado
          
           //Buscar Estados que solo coincidan con el Pais ID

            string pais = dt.Rows[0]["PaisId"].ToString();

            EstadoBLL edo = new EstadoBLL();
            DataTable dt3 = new DataTable();

            dt3 = edo.SearchPaisUpdate(pais);

            ddlEstado.DataSource = dt3;
            ddlEstado.DataTextField = "Nombre";  //Nombre en el dropdownlist
            ddlEstado.DataValueField = "EstadoId"; //Valor del campo en el dropdownlist
            ddlEstado.DataBind();

            //Selecionar el estado seleccionado 
            ddlEstado.Items.FindByValue(dt3.Rows[0]["EstadoId"].ToString()).Selected = true;


            //Cargar Pais
            
           cargarPaises();

            EstadoBLL edo4 = new EstadoBLL();
            DataTable dt4 = new DataTable();

            string edoid = dt3.Rows[0]["EstadoId"].ToString();

            dt4 = edo.consultarPais(edoid);

            ddlPais.Items.FindByValue(dt4.Rows[0]["PaisId"].ToString()).Selected = true;



            ////txtFacultad.Text = dt.Rows[0]["facultad"].ToString();
            //ddlFacultad.DataSource = dt;           //DataSource = Gets or sets the object from which the data-bound control retrieves its list of data items.
            //ddlFacultad.DataTextField = "codigo";  // DataTextField = Gets or sets the field of the data source that provides the text content of the list items.
            //ddlFacultad.DataValueField = "FacultadId"; //DataValueField= Gets or sets the field of the data source that provides the value of each list item.
            //ddlFacultad.DataBind(); //Dibujar tabla
            ////ddlFacultad.Items.Insert(1, new ListItem("Ver mas...", string.Empty));
         
        }
       protected void BuscarAlumno(string matricula)
       {

           AlumnoBLL alumno4 = new AlumnoBLL();
           DataTable dt = new DataTable();

           dt = alumno4.FindAlumno(matricula);
          
           if (dt != null && dt.Rows.Count > 0)
           {
               txtMatricula.Text = dt.Rows[0]["numMatricula"].ToString();
               txtNombre.Text = dt.Rows[0]["nombre"].ToString();
               txtSemestre.Text = dt.Rows[0]["semestre"].ToString();

               cargarFacultades();
               //cargarMunicipios();
               //cargarEstados();
               //cargarPaises();

               ddlFacultad.Items.FindByValue(dt.Rows[0]["facultad"].ToString()).Selected = true;

               //Buscar Muicipios que solo coinidan  con el EstadoId 

               string estado = dt.Rows[0]["estado"].ToString();

               MunicipioBLL mun = new MunicipioBLL();
               DataTable dt2 = new DataTable();

               dt2 = mun.SearchEstadoUpdate(estado);

               ddlMunicipio.DataSource = dt2;
               ddlMunicipio.DataTextField = "Nombre";  //Nombre en el dropdownlist
               ddlMunicipio.DataValueField = "MunId"; //Valor del campo en el dropdownlist
               ddlMunicipio.DataBind();

               //Selecionar el municipio seleccionado 
               ddlMunicipio.Items.FindByValue(dt.Rows[0]["MunId"].ToString()).Selected = true;


               string fecha = dt.Rows[0]["fechaNacimiento"].ToString();
               DateTime fechax = Convert.ToDateTime(fecha);
               txtFechaNac.Text = fechax.ToString("yyyy/MM/dd");


               //Cargar Estado

               //Buscar Estados que solo coincidan con el Pais ID

               string pais = dt.Rows[0]["PaisId"].ToString();

               EstadoBLL edo = new EstadoBLL();
               DataTable dt3 = new DataTable();

               dt3 = edo.SearchPaisUpdate(pais);

               ddlEstado.DataSource = dt3;
               ddlEstado.DataTextField = "Nombre";  //Nombre en el dropdownlist
               ddlEstado.DataValueField = "EstadoId"; //Valor del campo en el dropdownlist
               ddlEstado.DataBind();

               //Selecionar el estado seleccionado 
               ddlEstado.Items.FindByValue(dt3.Rows[0]["EstadoId"].ToString()).Selected = true;


               //Cargar Pais

               cargarPaises();

               EstadoBLL edo4 = new EstadoBLL();
               DataTable dt4 = new DataTable();

               string edoid = dt3.Rows[0]["EstadoId"].ToString();

               dt4 = edo.consultarPais(edoid);

               ddlPais.Items.FindByValue(dt4.Rows[0]["PaisId"].ToString()).Selected = true;


               // txtFechaNac.Text = dt.Rows[0]["fechaNacimiento"].ToString();
               //txtFacultad.Text = dt.Rows[0]["facultad"].ToString();

           }
           else 
           {
               ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('El Alumno no existe');window.location='" + Request.ApplicationPath + "Alumnos/alumno_s.aspx';", true);
               
           }

       }
  

       protected void modificarAlumno()
       {
           string matricula = txtMatricula.Text;
           string nombre = txtNombre.Text;
           string fechaNacimiento = txtFechaNac.Text;
           int? semestre;
           string facultad = ddlFacultad.SelectedValue;
           string municipio = ddlMunicipio.SelectedValue;


          //Checar semestre si tiene cosas
           if (txtSemestre.Text.Length > 0)
           {
               semestre = int.Parse(txtSemestre.Text);

           }
           else
           { 
                 semestre = null;
           }


           AlumnoBLL alumno = new AlumnoBLL();
           alumno.modificar(matricula, nombre, semestre, fechaNacimiento, facultad, municipio);
           ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('Alumno modificado exitosamente');window.location='" + Request.ApplicationPath + "Alumnos/alumno_s.aspx';", true);

       }
       
        #endregion





    }
}