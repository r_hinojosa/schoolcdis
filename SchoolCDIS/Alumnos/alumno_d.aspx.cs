﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SchoolCDIS;
using CapaLogica;

namespace SchoolCDIS.Alumnos
{
    public partial class alumno_d : System.Web.UI.Page
    {
        
        private int alumnoId=0;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["pId"] != null)
                {

                    alumnoId = int.Parse(Request.QueryString["pId"]);
                    buscarAlumnoPorId(alumnoId);
                    //cargarFacultades();

                }

            }
        }

        #region Eventos
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string matricula = txtMatricula.Text;
            BuscarAlumno(matricula);

        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarAlumno();

        }

        #endregion

        #region Metodos

        private void cargarPaises()
        {
            PaisBLL fac = new PaisBLL();
            DataTable dt = new DataTable();

            dt = fac.consultar();

            ddlPais.DataSource = dt;
            ddlPais.DataTextField = "Nombre";
            ddlPais.DataValueField = "PaisId";
            ddlPais.DataBind();
            ddlPais.Items.Insert(0, new ListItem("<-Seleccione->", ""));


        }

        private void cargarEstados()
        {
            EstadoBLL edo = new EstadoBLL();
            DataTable dt = new DataTable();

            dt = edo.consultar();

            ddlEstado.DataSource = dt;
            ddlEstado.DataTextField = "Nombre";
            ddlEstado.DataValueField = "EstadoId";
            ddlEstado.DataBind();
            ddlEstado.Items.Insert(0, new ListItem("<-Seleccione->", ""));
        }

        private void cargarMunicipios()
        {
            MunicipioBLL mun1 = new MunicipioBLL();
            DataTable dt = new DataTable();

            dt = mun1.consultar();

            ddlMunicipio.DataSource = dt;
            ddlMunicipio.DataTextField = "Nombre";
            ddlMunicipio.DataValueField = "MunId";
            ddlMunicipio.DataBind();
            ddlMunicipio.Items.Insert(0, new ListItem("<-Seleccione->", ""));
        }

        public void cargarFacultades()
        {

            AlumnoBLL alumno3 = new AlumnoBLL();
            DataTable dt = new DataTable();

            dt = alumno3.CargarFacultad();

            ddlFacultad.DataSource = dt;
            ddlFacultad.DataTextField = "codigo";
            ddlFacultad.DataValueField = "FacultadId";
            ddlFacultad.DataBind(); //Dibujar tabla
        }

        private void BuscarAlumno(string matricula)
        {
            AlumnoBLL alumno4 = new AlumnoBLL();
            DataTable dt = new DataTable();
            dt = alumno4.FindAlumno(matricula);
            if (dt != null && dt.Rows.Count > 0)
            {
                txtMatricula.Text = dt.Rows[0]["numMatricula"].ToString();
                txtNombre.Text = dt.Rows[0]["nombre"].ToString();
                txtSemestre.Text = dt.Rows[0]["semestre"].ToString();
                
                cargarFacultades();
                cargarEstados();
                cargarMunicipios();
                cargarPaises();

                ddlFacultad.Items.FindByValue(dt.Rows[0]["facultad"].ToString()).Selected = true;
                ddlMunicipio.Items.FindByValue(dt.Rows[0]["MunId"].ToString()).Selected = true;
                //ddlFacultad.Items.Insert(0, dt.Rows[0]["codigo"].ToString());
                string fecha = dt.Rows[0]["fechaNacimiento"].ToString();
                DateTime fechax = Convert.ToDateTime(fecha);
                txtFechaNac.Text = fechax.ToString("MM/dd/yyyy");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('El Alumno no existe');window.location='" + Request.ApplicationPath + "Alumnos/alumno_s.aspx';", true);
            
            }
          

        }

        private void EliminarAlumno()
        {

            string matricula = txtMatricula.Text;
            AlumnoBLL alumno5 = new AlumnoBLL();
            DataTable dt = new DataTable();
            dt = alumno5.EliminarAlumno(matricula);
            ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('Se ha eliminado el alumno exitosamente');window.location='" + Request.ApplicationPath + "Alumnos/alumno_s.aspx';", true);
            limpiarCampos();
        
        }

        protected void buscarAlumnoPorId(int id)
        {

            AlumnoBLL alumno2 = new AlumnoBLL();
            DataTable dt = new DataTable();
            dt = alumno2.BuscarId(id);

            txtMatricula.Text = dt.Rows[0]["numMatricula"].ToString();
            txtNombre.Text = dt.Rows[0]["nombre"].ToString();
            txtSemestre.Text = dt.Rows[0]["semestre"].ToString();
            cargarFacultades();

            cargarFacultades();
            cargarEstados();
            cargarMunicipios();
            cargarPaises();

            ddlFacultad.Items.FindByValue(dt.Rows[0]["facultad"].ToString()).Selected = true;
            ddlMunicipio.Items.FindByValue(dt.Rows[0]["MunId"].ToString()).Selected = true;
            
            //Cargar Estado

            MunicipioBLL mun = new MunicipioBLL();
            DataTable dt2 = new DataTable();

            string munid = dt.Rows[0]["MunId"].ToString();

            dt2 = mun.consultarEstadoUpdate(munid);

            ddlEstado.Items.FindByValue(dt2.Rows[0]["EstadoId"].ToString()).Selected = true;

            //Cargar Pais

            EstadoBLL edo = new EstadoBLL();
            DataTable dt3 = new DataTable();

            string edoid = dt2.Rows[0]["EstadoId"].ToString();

            dt3 = edo.consultarPais(edoid);

            ddlPais.Items.FindByValue(dt3.Rows[0]["PaisId"].ToString()).Selected = true;


           // ddlFacultad.Items.Insert(0, dt.Rows[0]["codigo"].ToString());
            string fecha = dt.Rows[0]["fechaNacimiento"].ToString();
            DateTime fechax = Convert.ToDateTime(fecha);
            txtFechaNac.Text = fechax.ToString("MM/dd/yyyy");

        }
        private void limpiarCampos()
        {
            //txtFacultad.Text = "";
            txtFechaNac.Text = "";
            txtMatricula.Text = "";
            txtNombre.Text = "";
            txtSemestre.Text = "";
        }

        #endregion 

     
    }
}