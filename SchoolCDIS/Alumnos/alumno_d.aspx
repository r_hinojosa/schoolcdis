﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="alumno_d.aspx.cs" Inherits="SchoolCDIS.Alumnos.alumno_d" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="/Styles/Site.css" rel="stylesheet" type="text/css" />
<div>
    <br />
    <p align="center"  style ="font-size:25px; font-family:'Trebuchet MS';color:black; background-color:aqua; ">
              Eliminar Alumno
        </p>
    <br />
    <br />

</div>
<table id="tablaAltas" align="center">
<tr>
<td><asp:Label ID="lblMatricula" runat="server" Text="Matricula" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td><asp:TextBox ID="txtMatricula" runat="server"></asp:TextBox></td>
<td> 
    &nbsp;</td>
</tr>
<tr>
<td><asp:Label ID="lblNombre" runat="server" Text="Nombre" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td><asp:TextBox ID="txtNombre" runat="server" Enabled ="false"></asp:TextBox></td>
<td>                                           
    &nbsp;</td>
</tr>
<tr>
<td><asp:Label ID="lblFechaNac" runat="server" Text="Fecha de nacimiento" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td><asp:TextBox ID="txtFechaNac" runat="server" Enabled ="false"></asp:TextBox></td>
</tr>
<tr>
<td><asp:Label ID="lblSemestre" runat="server" Text="Semestre" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td><asp:TextBox ID="txtSemestre" runat="server" Enabled ="false"></asp:TextBox></td>
</tr>
<tr>
<td><asp:Label ID="lblFacultad" runat="server" Text="Facultad" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<%--<td><asp:TextBox ID="txtFacultad" runat="server" Enabled ="false"></asp:TextBox></td>--%>
<td>
    <asp:DropDownList ID="ddlFacultad" runat="server" AutoPostBack="true" Enabled ="false" Width="132px">
    </asp:DropDownList>
</td>
</tr>
     <tr><td>
    <asp:Label ID="lblPais" runat="server" Text="Pais"></asp:Label></td>
    <td>
        <asp:DropDownList ID="ddlPais" runat="server" Height="19px" 
            Width="151px" Enabled="false">
        </asp:DropDownList>    </td>
    </tr>

    <tr><td>
    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label></td>
    <td>
        <asp:DropDownList ID="ddlEstado" runat="server" Height="19px" 
             Width="151px" Enabled ="false">
        </asp:DropDownList>    </td>
    </tr>

    <tr><td>
    <asp:Label ID="lblMunicipio" runat="server" Text="Municipio"></asp:Label></td>
    <td>
        <asp:DropDownList ID="ddlMunicipio" runat="server" Height="19px" 
             Width="151px" Enabled ="false">
        </asp:DropDownList>    </td>
    </tr>


<tr>
<td align = "center"> 
    &nbsp;&nbsp; 
    <asp:Button ID="btnBuscar"  runat="server" Text="Buscar" 
        onclick="btnBuscar_Click" Width="77px" BackColor="#339933" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="White" /></td>
<td align = "center">
    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" onclick="btnEliminar_Click" BackColor="#CC3300" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="White" 
        /></td>
</tr>
</table>
<div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</div>
</asp:Content>
