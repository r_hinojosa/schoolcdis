﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CapaLogica;
using System.Text.RegularExpressions;
using oAlumno;

namespace SchoolCDIS.Alumnos
{
    public partial class Alumno_i : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                cargarFacultades();
                cargarPaises();
                crearTabla();
            
            }
        }

        private void crearTabla()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("numMatricula");
            dt.Columns.Add("nombre");
            dt.Columns.Add("fechaNacimiento");
            dt.Columns.Add("semestre");
            dt.Columns.Add("facultad");
            dt.Columns.Add("Municipio");
           
            ViewState["dtAlumno"] = dt;

        }
        #region Eventos

        protected void cvFechaNac_ServerValidate(object sender, ServerValidateEventArgs e)
        {
          //Pasar javascript aqui 
            string fecha = e.Value;
            string expReg = @"^((19|20)[0-9][0-9])/((0[1-9]|1[012]))/(0[1-9]|[12][0-9]|3[01])$";
            Match match = Regex.Match(fecha, expReg);
        
           //  if (!/^(19|20)\d\d[\/](0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])$/.test(fecha))
            if (!match.Success)
            {
                e.IsValid = false;
            }
                
            else 
               
            {
                    char[] delimitador= { '/' };
                    string[] parts = fecha.Split(delimitador);
                    int day = int.Parse(parts[2]);
                    int month = int.Parse(parts[1]);
                    int year = int.Parse(parts[0]);
                    
                    int[] monthLength = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
           

                    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                    {
                        monthLength[1] = 29;
                    }
                    if (day > 0 && day <= monthLength[month - 1]) 
                    {
                        e.IsValid = true;

                    }
                    else 
                    {
                        e.IsValid = false;
                    }
            }


        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            // forzar validacion 

                if (cvFechaNac.IsValid)
                {
                    guardarAlumno();
                }
           
        }

        protected void ddlPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPais.SelectedIndex != 0)
            {
                //Limpiar primero estados
                ddlEstado.Items.Clear();
                ddlMunicipio.Items.Clear();
                
                //Cargamos estados 
                cargarEstados();

                ddlEstado.Enabled = true;
                ddlMunicipio.Enabled = false;

            }
            
            else
            {

                ddlEstado.Items.Clear();
                ddlMunicipio.Items.Clear();
                ddlEstado.Enabled = false;
                ddlMunicipio.Enabled = false;
            }

        }


        protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlMunicipio.SelectedIndex != 0)
            {
                //Limpiar primero municipios
                ddlMunicipio.Items.Clear();

                //Cargamos municipios
                cargarMunicipios();

                ddlMunicipio.Enabled = true;

            }
            else 
            { 
                ddlMunicipio.Enabled = false; 
            
            }

        }

   

        #endregion
       
        #region Metodos

        private void guardarAlumno()
        {
            
           //Variables refactorizadas 
          
            ol olx = new ol();
            olx.Matricula = txtMatricula.Text;
            olx.Nombre = txtNombre.Text;
            olx.FechaNacimiento = txtFechaNac.Text;
            olx.Semestre = txtSemestre.Text;
            olx.Facultad = ddlFacultad.SelectedValue;
            olx.Municipio = ddlMunicipio.SelectedValue;
            olx.Codigo = ddlFacultad.SelectedItem.Text;
            olx.NomMunicipio = ddlMunicipio.SelectedItem.Text;

            ////variables de los textbox
            //string matricula = txtMatricula.Text;
            //string nombre = txtNombre.Text;
            //string fechaNacimiento = txtFechaNac.Text;
            //string semestre = txtSemestre.Text;
            //string facultad = ddlFacultad.SelectedValue;
            //string municipio = ddlMunicipio.SelectedValue;
            //string codigo = ddlFacultad.SelectedItem.Text;
            //string nomMunicipio = ddlMunicipio.SelectedItem.Text;

                try
                {
               
                    //Buscar numero de alumnos en municipios
                    AlumnoBLL alumno4 = new AlumnoBLL();
                    alumno4.SaveAlumno(olx.Matricula, olx.Nombre, olx.Semestre, olx.FechaNacimiento, olx.Facultad, olx.Municipio);

                    DataTable dtAlumno = (DataTable)ViewState["dtAlumno"];
                    dtAlumno.Rows.Add(olx.Matricula, olx.Nombre, olx.FechaNacimiento, olx.Semestre, olx.Codigo, olx.NomMunicipio);
                    grdAlumno.DataSource = dtAlumno;
                    grdAlumno.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('Alumno guardado exitosamente')", true);
                    limpiarCampos();

                }

                catch (Exception e)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Exception", "alert('" + e.Message + "')", true);

                }


            }

        
        private void cargarFacultades()
        {
            AlumnoBLL alumno3 = new AlumnoBLL();
            DataTable dt = new DataTable();

            dt = alumno3.CargarFacultad();

            ddlFacultad.DataSource = dt;
            ddlFacultad.DataTextField = "codigo";
            ddlFacultad.DataValueField = "FacultadId";
            ddlFacultad.DataBind();
            ddlFacultad.Items.Insert(0, new ListItem("<-Seleccione->", ""));

        

        }

        private void cargarPaises()
        {
            PaisBLL fac = new PaisBLL();
            DataTable dt = new DataTable();

            dt = fac.consultar();

            ddlPais.DataSource = dt;
            ddlPais.DataTextField = "Nombre";
            ddlPais.DataValueField = "PaisId";
            ddlPais.DataBind();
            ddlPais.Items.Insert(0, new ListItem("<-Seleccione->", "0"));
        
        
        }

        private void cargarEstados()
        {

            EstadoBLL edo = new EstadoBLL();
            DataTable dt5 = new DataTable();

            string valorPais = ddlPais.SelectedValue;

            dt5 = edo.consultarPaisI(valorPais);


            ddlEstado.DataSource = dt5;
            ddlEstado.DataTextField = "Nombre";
            ddlEstado.DataValueField = "EstadoId";
            ddlEstado.DataBind();
            ddlEstado.Items.Insert(0, new ListItem("<-Seleccione->", ""));
          
           
        }

        private void cargarMunicipios()
        {
            MunicipioBLL mun = new MunicipioBLL();
            DataTable dt = new DataTable();
            
            string valorEstado = ddlEstado.SelectedValue;

            dt = mun.consultarEstado(valorEstado);

            ddlMunicipio.DataSource = dt;
            ddlMunicipio.DataTextField = "Nombre";
            ddlMunicipio.DataValueField = "MunId";
            ddlMunicipio.DataBind();
            ddlMunicipio.Items.Insert(0, new ListItem("<-Seleccione->", "0"));
        
           
        }


        private void limpiarCampos()
        {
            //txtFacultad.Text = "";
            txtFechaNac.Text = "";
            txtMatricula.Text = "";
            txtNombre.Text = "";
            txtSemestre.Text = "";
        }
        #endregion


  

 
      

    }
}