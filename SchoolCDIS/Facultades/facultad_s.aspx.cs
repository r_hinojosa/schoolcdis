﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CapaLogicaFacultad;

namespace SchoolCDIS
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        #region Eventos

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cargarFacultad();
            }
            
        }
        #endregion

        #region Metodos

        private void cargarFacultad()
        {

            FacultadBLL fac5 = new FacultadBLL();
            DataTable dt = new DataTable();
            dt = fac5.LoadFac();
            grdFacultad.DataSource = dt;
            grdFacultad.DataBind();

        }

        protected void grdFacultad_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Modificar")
            {
                Response.Redirect("~/Facultades/facultad_u.aspx?pId=" + e.CommandArgument.ToString());
            }
            if (e.CommandName == "Eliminar")
            {
                Response.Redirect("~/Facultades/facultad_d.aspx?pId=" + e.CommandArgument.ToString());

            }
        }
        
        #endregion

       

    }
}