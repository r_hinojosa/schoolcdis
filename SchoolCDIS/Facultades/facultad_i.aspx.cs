﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CapaLogicaFacultad;

namespace SchoolCDIS.Facultades
{
    public partial class facultad_i : System.Web.UI.Page
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAceptar2_Click(object sender, EventArgs e)
        {
           
            guardarFacultad();
        }

        
        #endregion
        #region Metodos
        protected void guardarFacultad()
        {
            string facultad = txtnomFacultad.Text;
            string codigo = txtcodigo.Text;

            //Hacemos busqueda primero para ver si ay coincidencias en los codigos

           FacultadBLL fac9 = new FacultadBLL();
            DataTable dt = new DataTable();
            dt = fac9.FindByCod(codigo);

            try
            {
                FacultadBLL fac4 = new FacultadBLL();
                fac4.SaveFac(facultad, codigo);
                ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('Facultad agregado exitosamente');window.location='" + Request.ApplicationPath + "Facultades/facultad_s.aspx';", true);
                limpiarCampos();
            }

            catch (Exception e)
            {

                ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('"+e.Message+"')", true);
            }
       
     
        }
        private void limpiarCampos()
        {
            txtnomFacultad.Text = "";
            txtcodigo.Text = "";
           
        }
        #endregion
       
    }
}