﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="facultad_i.aspx.cs" Inherits="SchoolCDIS.Facultades.facultad_i" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div>
    <br />
    <p align="center"  style ="font-size:25px; font-family:'Trebuchet MS';color:black; background-color:aqua; margin-top: 25px;">
              Registrar Facultad
        </p>
<br />
<br />
<br />

</div>
<table align ="center" id="tablaAltas">
<tr>
<td><asp:Label ID="lblnomFacultad" runat="server" Text="Nombre de Facultad" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td><asp:TextBox ID="txtnomFacultad" runat="server"></asp:TextBox></td>
    <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate ="txtnomFacultad" runat="server" ErrorMessage="Nombre de Facultad requerido" ForeColor="Red"  Text="*" ValidationGroup ="fac"></asp:RequiredFieldValidator></td>
</tr>
<tr>
<td><asp:Label ID="lblcodigo" runat="server" Text="Codigo" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td><asp:TextBox ID="txtcodigo" runat="server"></asp:TextBox></td>
    <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate ="txtcodigo" runat="server" ErrorMessage="Codigo requerido" ForeColor="Red"  Text="*" ValidationGroup ="fac"></asp:RequiredFieldValidator>  </td>
</tr>

<tr>
<td></td>
<td align = "right">
    <asp:Button ID="btnAceptar2" runat="server" Text="Aceptar" 
        onclick="btnAceptar2_Click" BackColor="#339933" ValidationGroup ="fac" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="White" />
</td>
</tr>
</table>
    <br />
    <asp:ValidationSummary ID="ValidationSummary1" Font-Bold="True" runat="server"   align ="center"  HeaderText="Debe de entrar los siguientes campos:" DisplayMode="List" ForeColor="Red" ValidationGroup ="fac"/>
</asp:Content>
