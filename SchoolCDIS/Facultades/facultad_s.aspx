﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="facultad_s.aspx.cs" Inherits="SchoolCDIS.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <br />
        <p align="center"  style ="font-size:25px; font-family:'Trebuchet MS';color:black; background-color:aqua; margin-top: 25px;">
              Tabla de Facultades
        </p>
        <br />
        <br />
         <br />

    </div>
    <asp:GridView ID="grdFacultad" runat="server" AutoGenerateColumns = "false" 
        onrowcommand="grdFacultad_RowCommand" Width="494px" align=center Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="White" HorizontalAlign="Center">
        <AlternatingRowStyle BackColor="#0033CC" />
     <Columns>
   <%-- <asp:TemplateField>
    <ItemTemplate>
    <asp:LinkButton ID="btnModificar" runat ="server" Text="Modificar"  CommandName = "Modificar" CommandArgument = '<%# Eval("facultadId")%>'  >
    </asp:LinkButton>
    </ItemTemplate>
    </asp:TemplateField>
     <asp:TemplateField>
     <ItemTemplate>
    <asp:LinkButton ID="btnEliminar" runat ="server" Text="Eliminar"  CommandName = "Eliminar" CommandArgument = '<%# Eval("facultadId")%>'  >
    </asp:LinkButton>
    </ItemTemplate>
    </asp:TemplateField>--%>


     <asp:TemplateField>
    <ItemTemplate>
    <asp:ImageButton ID="btnEliminar" Width="20px" Height="20px" runat="server" CommandName = "Eliminar" CommandArgument = '<%# Eval("facultadId")%>' ImageUrl="~/Imagenes/eliminar.png"  />
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <ItemTemplate>
    <asp:ImageButton ID="btnModificar" Width="20px" Height="20px" runat="server" CommandName = "Modificar" CommandArgument = '<%# Eval("facultadId")%>' ImageUrl="~/Imagenes/editar.jpg"  />
    </ItemTemplate>
    </asp:TemplateField>


   
    <asp:BoundField DataField ="facultadId" HeaderText="Id" Visible="false"/>
    <asp:BoundField DataField ="nomfacultad" HeaderText="Nombre de facultad" />
    <asp:BoundField DataField ="codigo" HeaderText="Codigo" />
  
    </Columns>
        <EditRowStyle BackColor="Black" />
        <HeaderStyle BackColor="#000000" />
        <RowStyle BackColor="#333399" />
    </asp:GridView>
</asp:Content>
