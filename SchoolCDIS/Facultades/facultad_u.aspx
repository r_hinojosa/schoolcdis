﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="facultad_u.aspx.cs" Inherits="SchoolCDIS.Facultades.facultad_u" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            margin-left: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <br />
        <p align ="center" style ="  font-size:25px; font-family:'Trebuchet MS';color:black; background-color:aqua; margin-top:25px;" > Modificar Facultad</p>

    <br />
    <br />
    <br />

</div>
<table align="center" id="tablaAltas" >
<td><asp:Label ID="lblCodigo" runat="server" Text="Codigo" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td><asp:TextBox ID="txtCodigo" runat="server"></asp:TextBox></td>
<td><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ingrese el codigo" Text="*" ControlToValidate="txtCodigo" ForeColor="Red" ValidationGroup="fac"></asp:RequiredFieldValidator></td>
</tr>
<tr>
<td><asp:Label ID="lblNomFacu" runat="server" Text="Nombre de Facultad" Font-Bold="True" Font-Names="Trebuchet MS"></asp:Label></td>
<td><asp:TextBox ID="txtNomFacu" runat="server" ></asp:TextBox></td>
<td>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Ingrese el nombre de la facultad" Text="*" ValidationGroup="fac" ControlToValidate="txtNomFacu" ForeColor="Red"></asp:RequiredFieldValidator> </td>
</tr>
<td> <asp:Button ID="btnBuscar" runat="server" Text="Buscar" ValidationGroup="fac"
        onclick="btnBuscar_Click" Width="104px" BackColor="#339933" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="White"/></td>
<td align = "right">
    <asp:Button ID="btnModificar" runat="server" Text="Modificar" ValidationGroup="fac"
        onclick="btnModificar_Click" BackColor="#CC3300" Font-Bold="True" Font-Names="Trebuchet MS" ForeColor="White" Width="93px" CssClass="auto-style1"/></td>
</tr>
</table>
    <br />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Debe de entrar los siguientes campos" Font-Bold="true" ForeColor="Red" ValidationGroup="fac" align="center" DisplayMode="List"/>
</asp:Content>
