﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CapaLogicaFacultad;

namespace SchoolCDIS.Facultades
{
    public partial class facultad_u : System.Web.UI.Page
    {

        private int FacultadId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["pId"] != null)
                {

                    FacultadId = int.Parse(Request.QueryString["pId"]);
                    buscarFacultadPorId(FacultadId);

                }
            }
        }

        #region Eventos
        
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            buscarFac();
        }


        protected void btnModificar_Click(object sender, EventArgs e)
        {
            string facultad = txtNomFacu.Text;
            string codigo = txtCodigo.Text;
            modificarFac(facultad, codigo);
        }

      
        #endregion

        #region Metodos

        private void modificarFac(string facultad, string codigo)
        {

            FacultadBLL fac8 = new FacultadBLL();
            fac8.ModifyFac(facultad, codigo);

            ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('Facultad modificada exitosamente');window.location='" + Request.ApplicationPath + "Facultades/facultad_s.aspx';", true);
        }

        private void buscarFac()
        {
            string codigo = txtCodigo.Text;

            FacultadBLL fac6 = new FacultadBLL();
            DataTable dt = new DataTable();

            dt = fac6.FindByCod(codigo);

            if ( dt != null && dt.Rows.Count > 0)
            {
                txtNomFacu.Text = dt.Rows[0]["nomFacultad"].ToString();
                txtCodigo.Text = dt.Rows[0]["codigo"].ToString();
            }
            else
            {
            ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('La Facultad no existe');window.location='" + Request.ApplicationPath + "Facultades/facultad_s.aspx';", true);
            }
            
       }

        protected void buscarFacultadPorId(int id)
        {
            FacultadBLL fac7 = new FacultadBLL();
            DataTable dt = new DataTable();

            dt = fac7.FindFacId(id);

            txtNomFacu.Text = dt.Rows[0]["nomFacultad"].ToString();
            txtCodigo.Text = dt.Rows[0]["codigo"].ToString();
        }

        #endregion

    }

}
