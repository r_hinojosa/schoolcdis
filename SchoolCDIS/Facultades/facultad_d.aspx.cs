﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CapaLogicaFacultad;

namespace SchoolCDIS.Facultades
{
    public partial class facultad_d : System.Web.UI.Page
    {
        private int FacultadId = 0;
        protected void Page_Load(object sender, EventArgs e)

        {
            if (!IsPostBack)
            {
                if (Request.QueryString["pId"] != null)
                {

                    FacultadId = int.Parse(Request.QueryString["pId"]);
                    buscarFacultadPorId(FacultadId);

                }
            }
        }

        #region Eventos
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string Codigo = txtCodigo.Text;
            buscarFac(Codigo);
        }


        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            string codigo = txtCodigo.Text;
            EliminarFac(codigo);
        }

  
        #endregion

        #region Metodos

        protected void buscarFacultadPorId(int id)
        {
            FacultadBLL fac = new FacultadBLL();
            DataTable dt = new DataTable();
            dt = fac.FindFacId(id);

            txtNomFacu.Text = dt.Rows[0]["nomFacultad"].ToString();
            txtCodigo.Text = dt.Rows[0]["codigo"].ToString();
        }
        protected void buscarFac(string codigo)
        {
            FacultadBLL fac2 = new FacultadBLL();
            DataTable dt = new DataTable();
            dt = fac2.FindByCod(codigo);
            if (dt != null && dt.Rows.Count > 0)
            {

                txtNomFacu.Text = dt.Rows[0]["nomFacultad"].ToString();
                txtCodigo.Text = dt.Rows[0]["codigo"].ToString();
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('La Facultad no existe');window.location='" + Request.ApplicationPath + "Facultades/facultad_s.aspx';", true);
            }


        }
        protected void EliminarFac(string codigo)
        {
            FacultadBLL fac3 = new FacultadBLL();
            DataTable dt = new DataTable();
            dt = fac3.EraseFac(codigo);
            ClientScript.RegisterStartupScript(this.GetType(), "Agregar", "alert('Facultad eliminada exitosamente');window.location='" + Request.ApplicationPath + "Facultades/facultad_s.aspx';", true);
            limpiarCampos();
        }

        private void limpiarCampos()
        {
            txtCodigo.Text = "";
            txtNomFacu.Text = "";

        }


        #endregion

    }
}