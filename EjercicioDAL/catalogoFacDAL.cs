﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GlobalesDAL;

namespace CapaDatosFac
{
    public class catalogoFacDAL
    {
        #region facultad_d

        public DataTable BuscarFacId(int id)
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            SqlParameter paramId = new SqlParameter();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            con.ConnectionString = VGlobales.conexion;
           
            //com.CommandText = "select FacultadId, nomFacultad, codigo from Facultad where FacultadId = @Id";
            
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "sp_BuscarFacId";
            com.Parameters.AddWithValue("@id",id);
            
            com.Connection = con;
            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();

            return dt;

            ////Colocar nombre el parámetro
            //paramId.ParameterName = "@Id";

            ////Asignar valor al parámetro
            //paramId.Value = id;

            ////Agregar el parámetro al comando 
            //com.Parameters.Add(paramId);

        }

        public DataTable BuscarFaCod(string codigo)
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            SqlParameter paramCodigo = new SqlParameter();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            //string Codigo = txtCodigo.Text;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = "select FacultadId, nomFacultad, codigo from Facultad where codigo ='" + Codigo + "'";
            com.CommandText = "select FacultadId, nomFacultad, codigo from Facultad where codigo = @cod";
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "sp_BuscarFaCod";
            com.Parameters.AddWithValue("@cod", codigo);

            com.Connection = con;

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();

            return dt;

            ////Colocar nombre el parámetro
            //paramCodigo.ParameterName = "@cod";

            ////Asignar valor al parámetro
            //paramCodigo.Value = codigo;

            ////Agregar el parámetro al comando 
            //com.Parameters.Add(paramCodigo);
        }

        public DataTable EliminarFac(string codigo) 
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            //SqlParameter paramCod = new SqlParameter();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            //string codigo = txtCodigo.Text;

            con.ConnectionString = VGlobales.conexion;


            //com.CommandText = "DELETE FROM Facultad WHERE codigo='" + txtCodigo.Text + "'";
            //com.CommandText = "DELETE FROM Facultad WHERE codigo = @cod";
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "sp_EliminarFac";
            com.Parameters.AddWithValue("@cod",codigo);
            
            
            com.Connection = con;

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();

            return dt;

            ////Colocar nombre el parámetro
            //paramCod.ParameterName = "@cod";

            ////Asignar valor al parámetro
            //paramCod.Value = codigo;

            ////Agregar el parámetro al comando 
            //com.Parameters.Add(paramCod);
        
        
        }
        #endregion
        #region facultad_i
        
        public void GuardarFac(string facultad, string codigo)
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            
            //Declarar el objeto de tipo parámetro 
            //SqlParameter paramFac = new SqlParameter();
            //SqlParameter paramcodigo = new SqlParameter();

            con.ConnectionString = VGlobales.conexion;
            //(com.CommandText = "Insert Into Facultad(nomFacultad,codigo) values (@fac,@cod)";
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "sp_GuardarFac";
            com.Parameters.AddWithValue("@fac",facultad);
            com.Parameters.AddWithValue("@cod",codigo);
            com.Connection = con;


            con.Open();
            com.ExecuteNonQuery();
            con.Close();

            ////Colocar nombre el parámetro
            //paramFac.ParameterName = "@fac";
            //paramcodigo.ParameterName = "@cod";


            ////Asignar valor al parámetro
            //paramFac.Value = facultad;
            //paramcodigo.Value = codigo;


            ////Agregar el parámetro al comando 
            //com.Parameters.Add(paramFac);
            //com.Parameters.Add(paramcodigo);
        
        }
        #endregion

        #region facultad_s
        
        public DataTable cargarFac()
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = "select FacultadId, nomFacultad, codigo from Facultad";
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "sp_cargarFac";
      
            com.Connection = con;

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;
        
        
        }
       
        #endregion

        #region facultad_u
       //Se recicla codigo de  BuscarFaCod y BuscarFacId
        public void modificarFac(string facultad, string codigo)
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            
            //SqlParameter paramfac = new SqlParameter();
            //SqlParameter paramcod = new SqlParameter();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            con.ConnectionString = VGlobales.conexion;
            com.CommandText = " UPDATE Facultad SET nomFacultad = @fac, codigo = @cod WHERE  codigo = @cod";
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "sp_modificarFac";
            com.Parameters.AddWithValue("@fac", facultad);
            com.Parameters.AddWithValue("@cod", codigo);

            com.Connection = con;

            con.Open();
            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();

            ////Colocar nombre el parámetro
            ////paramfac.ParameterName = "@fac";
            ////paramcod.ParameterName = "@cod";

            ////Asignar valor al parámetro
            ////paramfac.Value = facultad;
            ////paramcod.Value = codigo;


            ////Agregar el parámetro al comando 
            ////com.Parameters.Add(paramcod);
            ////com.Parameters.Add(paramfac);
        
        }
        
        #endregion
    }
}
