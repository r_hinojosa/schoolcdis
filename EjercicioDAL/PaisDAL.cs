﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GlobalesDAL;

namespace CapaDeDatos
{
    public class PaisDAL
    {
        public DataTable consultar()
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            com.CommandText = "sp_PaisConsultar";
            com.Connection = con;

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;
        
        }

        public DataTable consultarEstadoParaPais()
        {


            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            com.CommandText = "sp_consultarEstadoParaPais";
            com.Connection = con;

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;
        
        
        }


    }
}
