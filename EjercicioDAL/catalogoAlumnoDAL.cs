﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GlobalesDAL;

namespace CapaDeDatos
{
    public class catalogoAlumnoDAL
    {
        #region alumno_u.alumno_s

        public DataTable consultar()
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = "SELECT alumnoId, numMatricula, a.nombre, fechaNacimiento, semestre, codigo Facultad, m.Nombre Municipio, e.EstadoId Municipio from Alumno a  JOIN  Facultad  f on f.FacultadId = a.facultad JOIN Municipio m on m.MunId = a.MunId  JOIN Estado e on m.EstadoId = e.EstadoId";
            com.CommandText = "sp_Alumno_s";
            com.Connection = con;

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;

        }

        public DataTable modificar(string matricula, string nombre, int? semestre, string fechaNacimiento, string facultad, string municipio)
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            //SqlParameter paramMat = new SqlParameter();
            //SqlParameter paramNom = new SqlParameter();
            //SqlParameter paramfechaNac = new SqlParameter();
            //SqlParameter paramSem = new SqlParameter();
            //SqlParameter paramFacultad = new SqlParameter();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = " update Alumno set numMatricula = " + txtMatricula.Text + ", nombre = '" + txtNombre.Text + "',fechaNacimiento = '" +txtFechaNac.Text + "', semestre = " + txtSemestre.Text + ", facultad = '" + txtFacultad.Text + "' WHERE  numMatricula ='" + txtMatricula.Text + "'";
            // com.CommandText = " UPDATE Alumno SET numMatricula = @mat, nombre = @nom, fechaNacimiento = @fnac, semestre = @sem, facultad = @fac, MunId = @mun FROM Alumno a JOIN Facultad  f on  a.facultad = f.FacultadId  AND  numMatricula = @mat";

            com.CommandText = "sp_Alumno_u";
            com.Connection = con;


            //Asignar valores al store procedure
            com.Parameters.AddWithValue("@mat", matricula);
            com.Parameters.AddWithValue("@nom",nombre);
            com.Parameters.AddWithValue("@fnac",fechaNacimiento);
            com.Parameters.AddWithValue("@sem",semestre);
            com.Parameters.AddWithValue("@fac",facultad);
            com.Parameters.AddWithValue("@mun", municipio);
            
            con.Open();
            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();
           
            return dt;
            ////Colocar nombre el parámetro
            //paramMat.ParameterName = "@mat";
            //paramNom.ParameterName = "@nom";
            //paramfechaNac.ParameterName = "@fnac";
            //paramSem.ParameterName = "@sem";
            //paramFacultad.ParameterName = "@fac";

            ////Asignar valor al parámetro
            //paramMat.Value = matricula;
            //paramNom.Value = nombre;
            //paramfechaNac.Value = fechaNacimiento;
            //paramSem.Value = semestre;
            
            //paramFacultad.Value = facultad;

            ////Agregar el parámetro al comando 
            //com.Parameters.Add(paramMat);
            //com.Parameters.Add(paramNom);
            //com.Parameters.Add(paramfechaNac);
            //com.Parameters.Add(paramSem);
            //com.Parameters.Add(paramFacultad);


        }

        //CAMBIADIO EL QUERY POR NUEVAS TABLAS PAIS, ESTADO Y MUNICIPIO
        public DataTable FindById(int id)
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            //Declarar el objeto de tipo parámetro 
            SqlParameter paramId = new SqlParameter();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = "select alumnoId, numMatricula, nombre, fechaNacimiento, semestre, facultad from Alumno where alumnoId ='" + alumnoId + "'";
            // com.CommandText = "select alumnoId, numMatricula, nombre, fechaNacimiento, semestre, facultad from Alumno where alumnoId = @Id";
            // com.CommandText = "select * from Alumno a  JOIN  Facultad  f on f.FacultadId = a.facultad AND alumnoId = @Id";
            // NUEVO QUERY SELECT alumnoId, numMatricula, a.nombre, fechaNacimiento, semestre, facultad, a.MunId, f.codigo, m.Nombre Municipio, e.EstadoId Estado, p.PaisId from Alumno a  JOIN  Facultad  f on f.FacultadId = a.facultad JOIN Municipio m on m.MunId = a.MunId  JOIN Estado e on m.EstadoId = e.EstadoId JOIN Pais p on e.PaisId = p.PaisId AND alumnoId = @Id
            //VIEJO QUERY SELECT alumnoId, numMatricula, a.nombre, fechaNacimiento, semestre, facultad, a.MunId, f.codigo, m.Nombre Municipio, e.EstadoId Estado from Alumno a  JOIN  Facultad  f on f.FacultadId = a.facultad JOIN Municipio m on m.MunId = a.MunId  JOIN Estado e on m.EstadoId = e.EstadoId AND alumnoId = @Id";
            com.CommandText = "sp_AlumnoFindById";
            com.Connection = con;

            //Asignar valores al store procedure
            com.Parameters.AddWithValue("@id", id);

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();

            return dt;

            ////Colocar nombre el parámetro
            //paramId.ParameterName = "@Id";

            ////Asignar valor al parámetro
            //paramId.Value = id;

            ////Agregar el parámetro al comando 
            //com.Parameters.Add(paramId);

        }

        public DataTable cargarFacultades ()
        {
        
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();


            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;


            con.ConnectionString = VGlobales.conexion;
           // com.CommandText = "Select FacultadId,codigo from Facultad";
            com.CommandText = "sp_cargarFacultad";
            com.Connection = con;


            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();

            return dt;
        }

        //CAMBIADIO EL QUERY POR NUEVAS TABLAS PAIS, ESTADO Y MUNICIPIO

        public DataTable BuscarAlumnoU(string matricula )
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            //Declarar el objeto de tipo parámetro 
            SqlParameter paramMat = new SqlParameter();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = "select alumnoId, numMatricula, nombre, fechaNacimiento, semestre, facultad from Alumno where numMatricula ='" +matricula + "'";
            //com.CommandText = "SELECT alumnoId, numMatricula, nombre, fechaNacimiento, semestre, facultad, codigo, MunId from Alumno a  JOIN  Facultad  f on f.FacultadId = a.facultad AND numMatricula = @mat";
            //VIEJO QUERY SELECT alumnoId, numMatricula, a.nombre, fechaNacimiento, semestre, facultad, codigo, a.MunId, m.Nombre Municipio, e.EstadoId Municipio from Alumno a  JOIN  Facultad  f on f.FacultadId = a.facultad JOIN Municipio m on m.MunId = a.MunId  JOIN Estado e on m.EstadoId = e.EstadoId AND numMatricula = @mat
            // NUEVO QUERY SELECT alumnoId, numMatricula, a.nombre, fechaNacimiento, semestre, facultad,codigo, a.MunId, m.Nombre Municipio, e.EstadoId Estado, p.PaisId from Alumno a  JOIN  Facultad  f on f.FacultadId = a.facultad JOIN Municipio m on m.MunId = a.MunId  JOIN Estado e on m.EstadoId = e.EstadoId JOIN Pais p on e.PaisId = p.PaisId AND numMatricula = @mat
            com.CommandText = "sp_BuscarAlumnoU";
            com.Parameters.AddWithValue("@mat", matricula);

            com.Connection = con;
            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();

            return dt;
            ////Colocar nombre el parámetro
            //paramMat.ParameterName = "@mat";

            ////Asignar valor al parámetro
            //paramMat.Value = matricula;

            ////Agregar el parámetro al comando 
            //com.Parameters.Add(paramMat);
        
        }
        #endregion

        #region alumno_i

        public void guardarAlumnoi (string matricula, string nombre, string semestre, string fechaNacimiento, string facultad, string municipio)
        { 
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter();

            
            //Declarar el objeto de tipo parámetro 
            //SqlParameter paramMat = new SqlParameter();
            //SqlParameter paramNom = new SqlParameter();
            //SqlParameter paramfechaNac = new SqlParameter();
            //SqlParameter paramSem = new SqlParameter();
            //SqlParameter paramFacultad = new SqlParameter();

            con.ConnectionString = VGlobales.conexion;
            com.CommandType = CommandType.StoredProcedure;
            //com.CommandText = "Insert Into Alumno(numMatricula,nombre,fechaNacimiento,semestre,facultad) values (" +txtMatricula.Text + ",'"+txtNombre.Text+"','"+txtFechaNac.Text +"',"+txtSemestre.Text+",'"+txtFacultad.Text+"')";
            //com.CommandText = "Insert Into Alumno(numMatricula,nombre,fechaNacimiento,semestre,facultad) values (@mat,@nom,@fnac,@sem,@fac)";
            //com.CommandText = "INSERT INTO Alumno(numMatricula,nombre,fechaNacimiento,semestre,facultad) VALUES (@mat,@nom,@fnac,@sem,@fac)";
            // NUEVA QUERY INSERT INTO Alumno(numMatricula,nombre,fechaNacimiento,semestre,facultad,MunId) VALUES (@mat, @nom, @fnac, @sem, @fac,@mun)
            com.CommandText = "sp_GuardarAlumno";
            com.Connection = con;

            com.Parameters.AddWithValue("@mat",matricula);
            com.Parameters.AddWithValue("@nom",nombre);
            com.Parameters.AddWithValue("@fnac",fechaNacimiento);
            com.Parameters.AddWithValue("@sem", semestre);
            com.Parameters.AddWithValue("@fac", facultad);
            com.Parameters.AddWithValue("@mun", municipio);
            
            con.Open();
            com.ExecuteNonQuery();
            con.Close();

            ////Colocar nombre el parámetro
            //paramMat.ParameterName = "@mat";
            //paramNom.ParameterName = "@nom";
            //paramfechaNac.ParameterName = "@fnac";
            //paramSem.ParameterName = "@sem";
            //paramFacultad.ParameterName = "@fac";

            ////Asignar valor al parámetro
            //paramMat.Value = matricula;
            //paramNom.Value = nombre;
            //paramfechaNac.Value = fechaNacimiento;
            //paramSem.Value = semestre;
            //paramFacultad.Value = facultad;

            ////Agregar el parámetro al comando 
            //com.Parameters.Add(paramMat);
            //com.Parameters.Add(paramNom);
            //com.Parameters.Add(paramfechaNac);
            //com.Parameters.Add(paramSem);
            //com.Parameters.Add(paramFacultad);

    
          
        }

        //Funcion para devolver el numero de personas en un municipio
        public DataTable buscarNumMunicipios(string num)
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();


            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;


            con.ConnectionString = VGlobales.conexion;
            // SELECT COUNT(*) MunId FROM Alumno Where MunId  = @NumOfId
            com.CommandText = "sp_FindNumOfId";
            com.Connection = con;

            com.Parameters.AddWithValue("@NumOfId", num);


            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();

            return dt;
        }


        #endregion 

        #region alumno_d
        public DataTable Eliminaralumno (string matricula)
        {
        
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            //SqlParameter paramMat = new SqlParameter();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

           // string matricula = txtMatricula.Text;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = "DELETE FROM Alumno WHERE numMatricula='" +txtMatricula.Text + "'";
           
            //com.CommandText = "DELETE FROM Alumno WHERE numMatricula = @mat";
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "sp_EliminarAlumno";
            com.Parameters.AddWithValue("@mat",matricula);
            com.Connection = con;

            con.Open();
            da.SelectCommand = com;
            da.Fill(dt);
            con.Close();

            return dt;

            ////Colocar nombre el parámetro
            //paramMat.ParameterName = "@mat";


            ////Asignar valor al parámetro
            //paramMat.Value = matricula;

            ////Agregar el parámetro al comando 
            //com.Parameters.Add(paramMat);
        }

        #endregion

    }
}
