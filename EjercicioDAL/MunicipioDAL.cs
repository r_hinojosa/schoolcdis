﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GlobalesDAL;

namespace CapaDeDatos
{
    public class MunicipioDAL
    {

        public DataTable consultar()
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            com.CommandText = "sp_MunicipioConsultar";
            com.Connection = con;

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;

        }


        // Este sirve para el metodo de insertar (registrar alumno)

        public DataTable consultarEstado(string id)
        { 
        
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;
        
            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = " SELECT m.MunId, m.Nombre, m.EstadoId, e.Nombre Estado  FROM Municipio m JOIN Estado e on m.EstadoId = e.EstadoId WHERE m.EstadoId = @idMun";
            com.CommandText = "sp_ConsultarEstado";
            com.Connection = con;
            com.Parameters.AddWithValue("@idMun", id);

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;
      
        }


       
        //Este sirve solo para eliminar alumno

        public DataTable consultarEstadoUpdate(string id)
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = " SELECT m.MunId, m.Nombre, m.EstadoId, e.Nombre Estado  FROM Municipio m JOIN Estado e on m.EstadoId = e.EstadoId WHERE m.MunId = @idMun";
            com.CommandText = "sp_ConsultarEstadoUpdate";
            com.Connection = con;
            com.Parameters.AddWithValue("@idMun", id);

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;

        }


        //Este sirve para el metodo de update alumno con estadoId = estado Id  NO BORRAR  
        public DataTable SearchEstadoUpdate(string estado)
        
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion; 
            //com.CommandText = "SELECT MunId, Nombre, EstadoId FROM Municipio Where EstadoId = @EdoId";
            com.CommandText = "sp_SearchEstadoUpdate";
            com.Connection = con;
            com.Parameters.AddWithValue("@EdoId", estado);

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;
    
        }

    }
}
