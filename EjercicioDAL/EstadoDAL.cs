﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GlobalesDAL;

namespace CapaDeDatos
{
    public class EstadoDAL
    {

        public DataTable consultar()
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            com.CommandText = "sp_EstadoConsultar";
            com.Connection = con;

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;

        }

        // NO MOVER CODIGO  

        public DataTable consultarPais(string idEdo)
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = "SELECT e.EstadoId, e.Nombre, e.PaisId, p.Nombre Pais  FROM Estado e JOIN Pais p on e.PaisId = p.PaisId WHERE e.EstadoId = @idEdo";
            com.CommandText = "sp_consultarPais";
            com.Connection = con;
            com.Parameters.AddWithValue("@idEdo", idEdo);

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;


        }

        //CODIGO PARA  LA PAGINA DE ALUMNO_I

        public DataTable consultarPaisI(string idEdo)
        {

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = "SELECT e.EstadoId, e.Nombre, e.PaisId, p.Nombre Pais  FROM Estado e JOIN Pais p on e.PaisId = p.PaisId WHERE p.PaisId= @idPais";
            com.CommandText = "sp_consultarPaisI";
            com.Connection = con;
            com.Parameters.AddWithValue("@idPais", idEdo);

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;
       
        
        }
        // USADO PARA ALUMNO_U 
        public DataTable SearchPaisUpdate(string pais)
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            com.CommandType = CommandType.StoredProcedure;

            con.ConnectionString = VGlobales.conexion;
            //com.CommandText = "SELECT e.EstadoId, e.Nombre, e.PaisId FROM Estado e WHERE e.PaisId = @PaisId
            com.CommandText = "sp_consultarPaisI";
            com.Connection = con;
            com.Parameters.AddWithValue("@idPais", pais);

            con.Open();

            da.SelectCommand = com;
            da.Fill(dt);

            con.Close();

            return dt;
        
        
        }
    }
}
